const Config = {
  dir: './../../content/articles',
  token: '',
  months: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
  localHost: 'http://localhost:8080/graphql/',
  publicHost: 'https://meugenom.com/graphql/'
}

export default Config
